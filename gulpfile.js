// Plugins
var gulp         = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    cssbeautify  = require('gulp-cssbeautify'),
    critical     = require('critical'),
    browserSync  = require('browser-sync').create(),
    sass         = require('gulp-sass'),
    filter       = require('gulp-filter'),
    iconfont     = require('gulp-iconfont'),
    consolidate  = require('gulp-consolidate'),
    sassGlob     = require('gulp-sass-glob'),
    changed      = require('gulp-changed'),
    uglify       = require('gulp-uglify'),
    rename       = require('gulp-rename'),
    concat       = require('gulp-concat'),
    sourcemaps   = require('gulp-sourcemaps'),
    async        = require('async'),
    cleanCSS     = require('gulp-clean-css'),
    imagemin     = require('gulp-imagemin'),
    runTimestamp = new Date().getTime();


// Config
var config = require('./config.json');

/**
 * @task sass
 * Compile files from scss
 */
gulp.task('sass', function () {
	const f = filter(['**', '!src/scss/templates/*.scss']);
  return gulp.src(config.path.src.scss)
	.pipe(f)
  .pipe(sassGlob())
	.pipe(changed('css'))
  .pipe(sass({
		indentWidth: 2
  , outputStyle: 'expanded'
	}))
  .pipe(cssbeautify({indent: '  '}))
  .pipe(autoprefixer())
  .pipe(gulp.dest(config.path.dest.css))
  .pipe(browserSync.stream())
});

/**
 * @task minifying
 */
gulp.task('minify-css',() => {
  return gulp.src(config.path.dest.css + '/style.css')
    .pipe(sourcemaps.init())
    .pipe(cleanCSS())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('dist/assets/css'));
});

 /**
 * @task Launch the Server (Browser Sync)
 * Launch the Server (Browser Sync)
 */
gulp.task('browser-sync', gulp.series(['sass'], function() {
  browserSync.init({
    online: true,
    proxy: config.browserSync.proxy,
  });
 }));


 /**
 * @task concat-js
 * concat js files
 */
 gulp.task('concat-js', function() {
  return gulp.src(config.path.src.js)
    .pipe(sourcemaps.init())
    .pipe(concat('all.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(config.path.dest.js))
    .pipe(browserSync.stream());
});

/**
 * @task js
 * Minify all.js
 */
gulp.task('minify-js', function() {
  return gulp.src(config.path.dest.js + '/all.js')
    // Minify the file
    .pipe(uglify())
    .pipe(rename({
      suffix: '.min'
    }))
    .pipe(gulp.dest(config.path.dest.js))
});



gulp.task('iconfont', function () {
  gulp.src('src/assets/icons/*.svg')
    .pipe(iconfont({
      fontName: 'icons',
			appendCodepoints: true,
      prependUnicode: false, // recommended option 
      formats: ['svg', 'ttf', 'eot', 'woff', 'woff2'], // default, 'woff2' and 'svg' are available 
      timestamp: runTimestamp, // recommended to get consistent builds when watching files 
     centerHorizontally: true,
     normalize: true,
    }))
    .on('glyphs', function (glyphs, options) {
      gulp.src('src/assets/scss/templates/_icons.scss')
        .pipe(consolidate('lodash', {
          glyphs: glyphs,
         fontName: options.fontName,
         fontPath: '../icons/',
				 fontDate: options.timestamp,
         className: 'icon',
        }))
        .pipe(gulp.dest('src/assets/scss'));
    })
    .pipe(gulp.dest(config.path.dest.iconSvg))
    .pipe(browserSync.stream());
});

/**
 * Optimized images
 */
gulp.task('img', function () {
  return gulp.src(config.path.src.img)
    .pipe(imagemin())
    .pipe(gulp.dest(config.path.dest.img))
});

/**
 * Watch files
 */
gulp.task('watch', function () {
  gulp.watch([config.path.src.scss], gulp.parallel(['sass']));
  gulp.watch([config.path.src.js], gulp.parallel(['concat-js']));
  gulp.watch([config.path.src.img], gulp.parallel(['img']));
  gulp.watch([config.path.src.iconSvg], gulp.parallel(['iconfont']));
});

/**
 * Default task, running just `gulp` will 
 * compile Sass files, launch BrowserSync & watch files.
 */
gulp.task('prod', gulp.parallel(['watch', 'minify-css', 'minify-js']));
gulp.task('default', gulp.parallel(['browser-sync', 'watch']));